source /usr/share/cachyos-fish-config/cachyos-config.fish


# SYSTEM
# Variables
set -gx PATH ~/.nimble/bin/ $PATH

# COMMANDS
alias init_cachy='\
	sudo pacman -S \
		yay \
		firefox \
		thunderbird \
		steam-native-runtime \
		signal-desktop \
		code \
		nodejs \
		smb4k \
		partitionmanager \
		clipgrab \
		kolourpaint \
	&&  \
	yay -S \
		pcloud-drive \
		imagewriter \
	&& \
	curl https://nim-lang.org/choosenim/init.sh -sSf | sh \
	&& \
	nimble install nimlsp \
	&& \
	nimble install yaml \
	&& \
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim \
	&& \
	sudo pacman -R \
		cachy-browser \
		alacritty'

alias pls='\
	pacman -Ss yay \
	&& \
	pacman -Ss nimble'

alias update='sudo pacman -Syu && yay -Syu'
alias mirrorupdate='sudo reflector --protocol https --sort score --save /etc/pacman.d/mirrorlist'
alias :q='exit'
alias c='clear'
alias neofetch='fastfetch'
alias fishconf='vim ~/.config/fish/config.fish'
alias vimconf='vim ~/.vimrc'
alias nvimconf='nvim ~/.config/nvim/init.lua'
alias grubconf='sudo vim /etc/default/grub'
alias grubupdate='sudo grub-mkconfig -o /boot/grub/grub.cfg'
alias sddmconf='sudo vim /etc/sddm.conf'
alias alacrittyconf='vim ~/.config/alacritty/alacritty.yml'

# DEVELOPMENT
## Nim
### Flubber
set -gx PATH ~/Documents/code/Nim/Flubber/ $PATH
#alias nimcurrent='cd ~/Documents/code/Nim/Flubber/'
#alias nimtestdir='cd ~/Desktop/NimTest/'
#alias nimdev='xdg-open https://nim-lang.org/docs/manual.html && cd ~/Documents/code/Nim/Flubber/ && vim -S Session.vim'
#alias nimbuild='cd ~/Documents/code/Nim/Flubber && nimble build && cp -r $PWD ~/pCloudDrive/code/Nim/$(basename $PWD)_$(date +%Y-%m-%d_%T)'
#alias nimtest='cd ~/Desktop/NimTest/ && ls && flubber'

### meddl
set -gx PATH ~/Documents/code/Nim/meddl/ $PATH
alias nimcurrent='cd ~/Documents/code/Nim/meddl/'
alias nimdev='xdg-open https://nim-lang.org/docs/manual.html \
	&& cd ~/Documents/code/Nim/meddl/ && vim -S Session.vim'
alias nimbuild='cd ~/Documents/code/Nim/meddl \
	&& nim c --outdir:$PWD -d:ssl src/meddl.nim \
	&& cp -r $PWD ~/pCloudDrive/code/Nim/$(basename $PWD)_$(date +%Y-%m-%d_%T)'
alias nimtest='cd ~/Documents/code/Nim/meddl && meddl'

### Constant stuff
alias nimpcloud='cd ~/pCloudDrive/code/Nim/'
alias nimrun='nimbuild && nimtest'

## V
alias vcurrent='cd ~/Documents/code/V/test/'
alias vrun='v run .'
